<?php

namespace Devisr\HTTP\Server;

use \Devisr\Utils\Arrays\ArrayObject;

/**
 * Easy header management, oject oriented style
 * 
 * @author Cythral <talen.fisher@cythral.com>
 */
class Headers extends ArrayObject {
    use \Devisr\Utils\Properties;

    /**
     * Constructs a new Headers object
     */
    public function __construct() {
        foreach(headers_list() as $header) {
            $this->array[strstr($header, ":", true)] = substr(strstr($header, ":"), 1);
        }
    }

    /**
     * ArrayAccess set hook, when a header is added to the internal array, this
     * sets the actual header
     *
     * @param string $header the name of the header to set
     * @param string|array $value the header value or values to set
     * @return void
     */
    public function offsetSet($header, $value) {
        parent::offsetSet($header, $value);

        header_remove($header);

        if(!is_array($value)) $value = [ $value ];
        foreach($value as $headerval) {
            header("{$header}: $headerval", false);
        }
    }

    /**
     * ArrayAccess unset hook, when a header is removed from the internal array,
     * this unsets the header
     *
     * @param string $header the header to remove
     * @return void
     */
    public function offsetUnset($header) {
        parent::offsetUnset($header);
        header_remove($header);
    }

    /**
     * Checks to see if the headers have been sent already
     *
     * @return boolean true if headers were already sent, false if not
     */
    protected function getSent(): bool {
        return headers_sent();
    }
}