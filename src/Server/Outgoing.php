<?php

namespace Devisr\HTTP\Server;

/**
 * Easily manage different aspects of making an HTTP response,
 * such as setting the response code and headers.
 * 
 * @author Cythral <talen.fisher@cythral.com>
 */
class Outgoing {
    use \Devisr\Utils\Properties;

    /**
     * Constructs a new outgoing object
     */
    public function __construct() {
        $this->headers = new Headers;
    }

    /**
     * Sets the http response code
     *
     * @param integer $code the response code to use
     * @return void
     */
    protected function setCode(int $code): void {
        http_response_code($code);
    }

    /**
     * Gets the currently set http response code
     *
     * @return integer the currently set http response code
     */
    protected function getCode(): int {
        return http_response_code();
    }

    /**
     * Performs a redirect and exits the script if $exit == true
     * 
     * @param string $location the location to redirect to
     * @param int $code the http response code to use (default is 301)
     * @param boolean $exit if true, the script will exit
     * @return void
     */
    public function redirect(string $location, int $code = 301, bool $exit = true) {
        $this->code = 301;
        $this->headers["location"] = $location;
        if($exit) exit;
    }
}