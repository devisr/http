<?php

namespace Devisr\HTTP\Server;

class Json extends Outgoing {
    public function __construct() {
        parent::__construct();
        $this->headers["content-type"] = "application/json";
    }

    public function send($data, int $code = 200, bool $exit = true) {
        $this->code = $code;
        echo json_encode($data);
        if($exit) exit;
    }
}