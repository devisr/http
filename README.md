# Devisr HTTP
This Devisr component provides helper classes for dealing with HTTP requests.  It provides both client and server utilities.

## Server

### Incoming
Devisr provides an easy and object oriented way to access headers and other HTTP aspects about an incoming HTTP request.  By default, it uses array elements from the $_SERVER superglobal. 

```php
use Devisr\HTTP\Server\Incoming;

$in = new Incoming;
echo $in->url; // https://cythral.com/about
echo $in->protocol; // https
echo $in->hostname; // cythral.com
echo $in->uri; // /about
```

You may also pass your own array instead of using $_SERVER to the constructor for testing/overrides

### Outgoing
The outgoing class provides an object oriented way to set headers and the response status code.

```php
use Devisr\HTTP\Server\Outgoing;

$out = new Outgoing;
$out->code = 400; // set status codes
$out->redirect("/"); // perform redirects
$out->headers["content-type"] = "text/plain"; // manipulate headers
```

### EventStream
Devisr provides an EventStream server:

```php
use Devisr\HTTP\Server\EventStream;

$out = new EventStream;
$out->send("progress", 70); // send named events
$out->send(1, 80); // send events with an id
$out->send(null, 90); // send events with just data
```

### Json
Devisr provides a Json HTTP server:

```php
use Devisr\HTTP\Server\Json;

$out = new Json;
$out->send([ "error" => "access_denied" ], 400); 
// status code optional, defaults to 200. 
// Pass false as the third parameter to keep the script from exiting
```

## Client

Devisr provides an HTTP client, acting as a wrapper for php's curl extension. When curl is not available, this falls back to using fsockopen.  It is recommended to use curl for performance reasons. This provides minimal options, if you are needing a more extensive HTTP client, you'll need to use something else for the time being.

```php
$request = new reqc\HTTP\Request($options);
var_dump($request->response);
```

where $options is an array that has the following key => value pairs:

- url* - the url to make the request to
- method - the HTTP method to use (GET/POST/etc.)
- headers - an array of headers to set
- json - bool value, whether or not to parse the request and response bodies as json
- data - an array of key value pairs to send as post fields or json data. If content-type is set to application/json or if the json mode is used, this will automagically be json encoded.
- handle-ratelimits - defaults to true. If true, the request will attempt to retry until a non-429 response code is received.
- max-attempts - defaults to 5. Sets the maximum amount of retry attempts to make if handling rate limits.

* indicates a required option


