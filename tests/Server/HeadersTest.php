<?php

use Devisr\HTTP\Server\Headers;
use PHPUnit\Framework\TestCase;

/**
 * @runTestsInSeparateProcesses
 */
class HeadersTest extends TestCase {

    public function tearDown() {
        header_remove();
    }

    public function testSetSingle() {
        $headers = new Headers;
        $headers["x-test"] = "a";
        $this->assertContains("x-test: a", xdebug_get_headers());
    }

    public function testSetMultiple() {
        $headers = new Headers;
        $headers["x-test"] = [ "a", "b" ];
        
        $this->assertContains("x-test: a", xdebug_get_headers());
        $this->assertContains("x-test: b", xdebug_get_headers());
    }
}