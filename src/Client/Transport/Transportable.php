<?php

namespace Devisr\HTTP\Client\Transport;

interface Transportable {
    public function setMethod(string $method): bool;
    public function setHeaders(array $headers): bool;
    public function setBody($body): bool;
}