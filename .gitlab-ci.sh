#!/bin/bash

# We need to install dependencies only for Docker
[[ ! -e /.dockerenv ]] && exit 0

set -xe

# Install git (the php image doesn't have it) which is required by composer
apt-get update -yqq
apt-get install curl git zip unzip -yqq

# setup apache
cp .gitlab/vhost.conf /etc/apache2/sites-enabled/gitlab.conf
sed -i "s/{root}/$(pwd | sed 's/\//\\\//g')/g" /etc/apache2/sites-enabled/gitlab.conf
service apache2 start

curl --location --output /usr/local/bin/phpunit https://phar.phpunit.de/phpunit.phar
chmod +x /usr/local/bin/phpunit

# Install mysql driver
# Here you can install any other extension that you need
pecl install xdebug
docker-php-ext-install pdo_mysql
docker-php-ext-enable xdebug

# Install Curl
curl --silent --show-error https://getcomposer.org/installer | php
chmod +x composer.phar
mv composer.phar /usr/local/bin/composer
composer --version