<?php

use PHPUnit\Framework\TestCase;
use Devisr\HTTP\Server\EventStream;

/**
 * @runTestsInSeparateProcesses
 */
class EventStreamTest extends TestCase {

    public function testNamedEvent() {
        $es = new EventStream();
        $this->expectOutputString("event:progress\ndata:10\n\n");
        $es->send("progress", 10);
    }

    public function testNamedEventWithJson() {
        $es = new EventStream();
        $this->expectOutputString("event:progress\ndata:{\"value\":10}\n\n");
        $es->send("progress", ["value" => 10]);
    }

    public function testIdEvent() {
        $es = new EventStream();
        $this->expectOutputString("id:1\ndata:test\n\n");
        $es->send(1, "test");
    }

    public function testDataEvent() {
        $es = new EventStream();
        $this->expectOutputString("data:test\n\n");
        $es->send(null, "test");
    }
}