<?php

use Devisr\HTTP\Server\Outgoing;
use PHPUnit\Framework\TestCase;

/**
 * @runTestsInSeparateProcesses
 */
class OutgoingTest extends TestCase {
    
    public function tearDown() {
        http_response_code(200);
    }

    public function testGetCode() {
        http_response_code(500);
        $out = new Outgoing;

        $this->assertEquals(500, $out->code);
    }

    public function testSetCode() {
        $out = new Outgoing;
        $out->code = 404;

        $this->assertEquals(404, http_response_code());
    }

    public function testRedirect() {
        $out = new Outgoing;
        $out->redirect("/");

        $this->assertContains("location: /", xdebug_get_headers());
        $this->assertEquals(301, http_response_code());
    }

    public function testRedirectCustomCode() {
        $out = new Outgoing;
        $out->redirect("/", 302);
        
        $this->assertEquals(302, http_response_code());
    }

}