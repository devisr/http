<?php

use \PHPUnit\Framework\TestCase;
use \Devisr\HTTP\Client\Request;
use \Devisr\HTTP\Client\Response;

class RequestTest extends TestCase {
	/**
	 * @dataProvider providerGetRequest
	 */
	public function testGetRequestContent($req) {
		$resp = $req->response;
		$this->assertEquals('hello world', (String)$req);
    }
    
	/**
	 * @dataProvider providerPostRequest
	 */
	public function testPostRequestContent($req) {
        $resp = $req->response;
		$this->assertEquals(["foo" => "bar"], json_decode($resp->body, true));
    }
    
	/**
	 * @dataProvider providerJsonRequest
	 */
	public function testJsonRequestContent($req) {
		$resp = $req->response;
        $body = $resp->body;

		$expectedBody = new stdClass;
        $expectedBody->foo = "bar";
        
		$this->assertEquals($expectedBody, $body);
    }
    
	/**
	 * @dataProvider providerRateLimitedRequest
	 */
	public function testRateLimitedRequestAttempts($req) {
		$this->assertTrue($req->done);
		$this->assertEquals(2, $req->attempts);
		$this->assertEquals("success", (String)$req);
    }
    
	/**
	 * @dataProvider providerAuthentication
	 */
	public function testAuthentication($req, $expectsuccess) {
		if($expectsuccess) {
			$this->assertEquals("success", $req->response->body);
		} else {
			$this->assertEquals("invalid", $req->response->body);
		}
	}
	
	public function providerGetRequest() {
		$options = ["url" => "http://localhost/srv/get.php"];
		return [
			[ new Request($options) ],
			[ new Request($options + ["use-fsockopen" => true]) ]
		];
    }
    
	public function providerPostRequest() {
		$options = [
			"url" => "http://localhost/srv/post.php",
			"method" => "POST",
			"data" => [ "foo" => "bar" ]
		];
		return [
			[ new Request($options) ],
			[ new Request($options + ["use-fsockopen" => true]) ]
		];
    }
    
	public function providerJsonRequest() {
		$options = [
			"url" => "http://localhost/srv/post.php",
			"method" => "POST",
			"json" => true,
			"data" => [ "foo" => "bar" ]
		];
		return [
			[ new Request($options) ],
			[ new Request($options + ["use-fsockopen" => true]) ]
		];
    }
    
	public function providerRateLimitedRequest() {
		$options = [ "url" => "http://localhost/srv/ratelimited.php" ];
		return [
			[ new Request($options) ],
			[ new Request($options + ["use-fsockopen" => true]) ]
		];
    }
    
	public function providerAuthentication() {
		$options = [ 
			"url" => "http://localhost/srv/authentication.php"
		];
		return [
			[ new Request($options + ["auth" => [ "type" => "basic", "user" => "root", "pass" => "password" ]]), true ],
			[ new Request($options + ["auth" => [ "type" => "basic", "user" => "root", "pass" => "passwordx" ]]), false ]
		];
	}
}