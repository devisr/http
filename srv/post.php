<?php

$input = file_get_contents('php://input');
$json = json_decode($input, true);

if(parse_url("?".$input, PHP_URL_QUERY)) parse_str($input, $_REQUEST);
if(json_last_error() == JSON_ERROR_NONE) $_REQUEST = $json;
    
header("content-type: application/json");
die(json_encode($_REQUEST, JSON_FORCE_OBJECT));