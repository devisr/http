<?php

namespace Devisr\HTTP\Server;

/**
 * A class for easy access to aspects about the incoming
 * HTTP request (what URL was requested, port, ip, etc.)
 * 
 * @author Cythral <talen.fisher@cythral.com>
 */
class Incoming {
    use \Devisr\Utils\Properties;

    private $server;
    private $pathParts;
    private $domainParts;

    /**
     * Constructs a new Incoming object
     *
     * @param array|null $server an array to use instead of $_SERVER
     */
    public function __construct(?array $server = null) {
        $this->server = $server ?? $_SERVER;
        $this->uri = strtok($this->server["REQUEST_URI"], "?");
        $this->qs = strtok("?");
        $this->path = (strpos($this->uri, ".")) ? strstr($this->uri, ".", true) : $this->uri;
        $this->pathParts = explode("/", $this->path);
        $this->directory = count($this->pathParts) > 2 ? implode("/", array_slice($this->pathParts, 0, -1)) : "/";
        $this->filename = array_reverse($this->pathParts)[0];
        $this->extension = (ltrim(strstr($this->uri, "."), "."));
        $this->extension = ($this->extension == "") ? null : $this->extension;
        $this->domainParts = array_reverse(explode(".", $this->hostName));
    }

    /**
     * Gets the version of the protocol (ie http 1.0/1.1/2)
     *
     * @return float the protocol version 
     */
    protected function getProtocolVersion(): float {
        return substr(strstr($this->server["SERVER_PROTOCOL"], "/"), 1);
    }

    /**
     * Gets the protocol used to access the resource
     *
     * @return string the resource protocol
     */
    protected function getProtocol(): string {
        $protocol = strtolower(strstr($this->server["SERVER_PROTOCOL"], "/", true));
        return $protocol == "http" && $this->ssl ? "https" : $protocol;
    }

    /**
     * Checks if SSL is in use
     *
     * @return boolean true if ssl was used to access the resource or false if not
     */
    protected function getSSL(): bool {
        return (bool)($this->server["SSL"] ?? false);
    }

    /**
     * The IP of the entity requesting the resource
     *
     * @return string|null the ip address used or null if unavailable
     */
    protected function getIP(): ?string {
        return $this->server["REMOTE_ADDR"] ?? null;
    }

    /**
     * Gets the port the resource is being accessed on
     *
     * @return int|null the port used to access the resource or null if unavailable
     */
    protected function getPort(): ?int {
        return (int)$this->server["PORT"] ?? null;
    }

    /**
     * Gets the HTTP method used ot access the resource.  GET is 
     * the default if the method is not available.
     *
     * @return string the HTTP method in use
     */
    protected function getMethod(): string {
        return strtoupper($this->server["REQUEST_METHOD"] ?? "GET");
    }

    /**
     * Gets the hostname that was requested
     *
     * @return string the host name that was requested or null if unavailable
     */
    protected function getHostName(): ?string {
        return $this->server["SERVER_NAME"] ?? null;
    }

    /**
     * Gets the URL without the query string that was requested
     *
     * @return string gets the url that was requested without a query string
     */
    protected function getUrl(): string {
        return "{$this->protocol}://{$this->hostName}{$this->uri}";
    }

    /**
     * Gets the url with the query string included that was requested.
     *
     * @return string gets the url that was requested with the query string
     */
    protected function getUrlWithQs() {
        return $this->qs ? "{$this->url}?{$this->qs}" : $this->url;
    }
}