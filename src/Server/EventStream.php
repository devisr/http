<?php

namespace Devisr\HTTP\Server;

class EventStream extends Outgoing {

    const KEY_PREFIX_MAP = [
        "string" => "event:",
        "integer" => "id:"
    ];

    public function __construct() {
        parent::__construct();
        $this->headers["content-type"] = "text/event-stream\n\n";
        $this->headers["cache-control"] = "no-cache";
    }

    public function send($key = null, $value) {
        if(is_array($value)) $value = json_encode($value);
        $this->flush("{$this->getPrefix($key)}data:{$value}\n\n");
    }

    public function flush(string $output) {
        echo $output;
        flush();
    }

    private function getPrefix($key) {
        return isset(self::KEY_PREFIX_MAP[gettype($key)]) ? self::KEY_PREFIX_MAP[gettype($key)]."{$key}\n" : "";
    }
}