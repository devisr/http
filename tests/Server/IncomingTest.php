<?php

use Devisr\HTTP\Server\Incoming;
use PHPUnit\Framework\TestCase;

class IncomingTest extends TestCase {
    /**
     * @dataProvider providerIncoming
     */
    public function testUri($incoming) {
        $this->assertEquals("/about/team", $incoming->uri);
    }

    /**
     * @dataProvider providerIncoming
     */
    public function testQs($incoming) {
        $this->assertEquals("a=b", $incoming->qs);
    }

    /**
     * @dataProvider providerIncoming
     */
    public function testDirectory($incoming) {
        $this->assertEquals("/about", $incoming->directory);
    }

    /**
     * @dataProvider providerIncoming
     */
    public function testFilename($incoming) {
        $this->assertEquals("team", $incoming->filename);
    }

    /**
     * @dataProvider providerIncoming
     */
    public function testExtension($incoming) {
        $this->assertEquals(null, $incoming->extension);
    }

    /**
     * @dataProvider providerIncoming
     */
    public function testProtocolVersion($incoming) {
        $this->assertEquals(2, $incoming->protocolversion);
    }

    /**
     * @dataProvider providerIncoming
     */
    public function testProtocol($incoming) {
        $this->assertEquals("http", $incoming->protocol);
    }

    /**
     * @dataProvider providerIncoming
     */
    public function testIp($incoming) {
        $this->assertEquals("127.0.0.1", $incoming->ip);
    }
    
    /**
     * @dataProvider providerIncoming
     */
    public function testMethod($incoming) {
        $this->assertEquals("POST", $incoming->method);
    }

    /**
     * @dataProvider providerIncoming
     */
    public function testHostname($incoming) {
        $this->assertEquals("cythral.com", $incoming->hostname);
    }

    /**
     * @dataProvider providerIncoming
     */
    public function testUrl($incoming) {
        $this->assertEquals("http://cythral.com/about/team", $incoming->url);
    }

    /**
     * @dataProvider providerIncoming
     */
    public function testUrlWithQs($incoming) {
        $this->assertEquals("http://cythral.com/about/team?a=b", $incoming->urlwithqs);
    }
    
    public function providerIncoming() {
        return [
            [ 
                new Incoming([
                    "SERVER_PROTOCOL" => "HTTP/2",
                    "REMOTE_ADDR" => "127.0.0.1",
                    "PORT" => "8080",
                    "SSL" => false,
                    "REQUEST_METHOD" => "POST",
                    "SERVER_NAME" => "cythral.com",
                    "REQUEST_URI" => "/about/team?a=b"
                ])
            ]
        ];
    }
}